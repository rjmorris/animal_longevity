This was completed as an entry in the [August 2019 Reddit DataIsBeautiful visualization contest]( https://www.reddit.com/r/dataisbeautiful/comments/cmrz6j/battle_dataviz_battle_for_the_month_of_august/). The challenge was to create a visualization of the heart rates, masses, and longevity of a small set of animals.

I decided to create two visualizations. I entered the first in the contest, and the second was mostly for my own amusement. All code for generating the visualizations can be found in the accompanying [Jupyter notebook](./create_plots.ipynb).

# Human longevity is an outlier

In plots of heart rate, mass, and longevity, a couple of observations stood out:

- Heart rate, mass, and longevity are all strongly correlated with each other.
- Humans appear to be an outlier in terms of their relationship between longevity and heart rate and their relationship between longevity and mass.

Given these observations, I thought it would be interesting to predict longevity from heart rate and mass, and then use the residuals to see just how big of an outlier humans are:

![observed vs. predicted longevity](./predicted_longevity.png)

So humans are the indeed biggest outlier under this model, living over 150% longer than the model predicted (that is, over 2.5 times the prediction). Chickens were next at over 75% longer than predicted, although it should be noted that chickens are the only non-mammal in the data.

There are several big disclaimers to this whole approach:

- The dataset includes a very small number of animals (15), so the relationships may not hold once more animals are added.
- It isn't clear how the animals were chosen for inclusion in the dataset, so there could be a significant sampling bias present.
- The data values are unrealistically precise. For example, elephants could vary in mass by 50%, but the dataset includes only a single average mass. This reinforces the concern about the small size of the dataset. Not only do we have a small number of animals, but we also have just one observation per animal, so we can't capture any variation within a species.

As a result of these points, I wouldn't trust any model based on this data. However, my goal for this contest entry wasn't statistical rigor; it was to produce a visualization. I decided to pretend that the model was reasonable and focus on a visualization one might use to communicate the model results.

# If your mother were a hamster...

> Your mother was a hamster, and your father smelt of elderberries.

This marvelous insult from _Monty Python and the Holy Grail_ inspired my second visualization. Because a hamster was one of the animals in the dataset, and because it represents the extreme value for all three measures, I thought it would be fun to view each measure relative to a hamster. With this visualization, you could answer a question like "If your mother were a hamster, how many times her mass would your mass be?"

![if your mother were a hamster](./vs_hamster.png)

Unlike my first visualization, this one displays all the original data in a fairly direct way. Perhaps the contest judges would have preferred a more direct presentation of the data, and so maybe I should have submitted this one instead. This point wasn't addressed in the contest rules.
